﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class SpellCorrections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Description", "DescriptionPl" },
                values: new object[] { "ASP.NET CORE 2.X", "ASP.NET CORE 2.X" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"),
                columns: new[] { "LongDescription", "LongDescriptionPl", "ShortDescription" },
                values: new object[] { "Asp.NET CORE 2.X with Razor/jQuery/Bootstrap4. Fun project which was kind of fantasy league during FIFA WorldCup 2018. A lot of statistics, mobile friendly.", "Aplikacja Asp.NET CORE 2.X z frontendem w Razorze/jQuery/Bootstrap4. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.", "Asp.NET CORE 2.X application. Fun project which was kind of fantasy league during FIFA WorldCup 2018" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Description", "DescriptionPl" },
                values: new object[] { "ASP.NET CORE 1.X", "ASP.NET CORE 1.X" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"),
                columns: new[] { "LongDescription", "LongDescriptionPl", "ShortDescription" },
                values: new object[] { "Asp.NET CORE 1.X with Razor/jQuery/Bootstrap4. Fun project which was kind of fantasy league during FIFA WorldCup 2018. A lot of statistics, mobile friendly.", "Aplikacja Asp.NET CORE 1.X z frontendem w Razorze/jQuery/Bootstrap4. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.", "Asp.NET CORE 1.X application. Fun project which was kind of fantasy league during FIFA WorldCup 2018" });
        }
    }
}
