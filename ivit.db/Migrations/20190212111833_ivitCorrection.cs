﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class ivitCorrection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946"),
                columns: new[] { "EndDate", "StartDate" },
                values: new object[] { new DateTime(2019, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2018, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946"),
                columns: new[] { "EndDate", "StartDate" },
                values: new object[] { new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2017, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }
    }
}
