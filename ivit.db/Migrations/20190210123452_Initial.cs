﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    ShortDescription = table.Column<string>(nullable: false),
                    LongDescription = table.Column<string>(nullable: false),
                    ShortDescriptionPl = table.Column<string>(nullable: false),
                    LongDescriptionPl = table.Column<string>(nullable: false),
                    Role = table.Column<string>(nullable: false),
                    RolePl = table.Column<string>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ProjectType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Percent = table.Column<int>(nullable: false),
                    EnglishText = table.Column<string>(nullable: true),
                    PolishText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimelineEvents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartYear = table.Column<int>(nullable: false),
                    EndYear = table.Column<int>(nullable: true),
                    EnglishText = table.Column<string>(nullable: true),
                    PolishText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimelineEvents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BackendTechnologies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    DescriptionPl = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackendTechnologies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackendTechnologies_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FrontendTechnologies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    DescriptionPl = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrontendTechnologies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FrontendTechnologies_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Libraries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    DescriptionPl = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Libraries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Libraries_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Others",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    DescriptionPl = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Others", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Others_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "EndDate", "LongDescription", "LongDescriptionPl", "ProjectType", "Role", "RolePl", "ShortDescription", "ShortDescriptionPl", "StartDate", "Title" },
                values: new object[,]
                {
                    { new Guid("a9fa3928-8d30-427d-a839-6a71f445884a"), new DateTime(2014, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Graphic editor for SVG files. It's dedicated to drawing rooms with numbered seats which a later exported to ticket selling system. It uses WinForms technology with default UI. The most interesting part of this project is image to SVG parser.", "Edytor graficzny plików SVG. Przeznaczony do tworzenia wizualizacji sal z miejscami numerowanymi. Pliki importowane są do systemu biletowego. Aplikacja została napisana w WinForms, a jej najciekawszym elementem jest parser image -> SVG.", 1, "Development/Design", "Programowanie/Projektowanie", "Graphic editor for SVG files.", "Edytor graficzny plików SVG.", new DateTime(2013, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "room Editor" },
                    { new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"), new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "WinForms application designed to automate part of the packaging process. It applies client best practices to loaded MSI file. It also has filesystem store of clients best practices. Additionaly it creates start batch/vbs scripts.", "Aplikacja służąca do wstepnej edycji plików MSI w procesie packagingu. Dodaje do nich dobre praktyki danego klienta. Dodatkowo posiada magazyn dobrych praktyk działający na systemie plików. Oprócz tego aplikacja tworzy pliki startowe (batch/vbs).", 1, "Development/Design", "Programowanie/Projektowanie", "Automation tools for software packaging process.", "Zestaw narzędzi automatyzujący część procesów przy paczkowaniu aplikacji.", new DateTime(2014, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Software Packaging Tools" },
                    { new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4"), new DateTime(2016, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asp.NET WebForms application. It uses AD users, MS SQL database and file system data import to manage user permissions to raports generated in QlikView. ", "Aplikacja Asp.NET Web Forms używająca użytkowników z AD, bazy danych MS SQL i systemu plików do importu danych. Aplikacja służy do zarządzania uprawnieniami użytkowników do raportów generowanych w QlikView.", 0, "Maintenance/Development", "Utrzymanie/Programowanie", "Permissions managment for QlikView application.", "Zarządzanie uprawnieniami do QlikView.", new DateTime(2015, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Portal QV" },
                    { new Guid("eb294000-d308-412a-a79b-2b27b987ae57"), new DateTime(2016, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "WPF desktop application. It is designed to generate JPK files from Excel templates. It also provides connectivity with MF gateway where you can send this files and recieve UPO.", "Aplikacja desktopowa WPF. Umożliwia generowanie plików JPK na podstawie wypełnionych szablonów z programu Excel. Dodatkowo gwarantuje połączenie z bramką ministerstwa finansów co pozwala wysłać te pliki i odebrać UPO.", 1, "Development/Design", "Programowanie/Projektowanie", "JPK files generator and sender.", "Program do wysyłki i genrowania plików JPK.", new DateTime(2015, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kreator JPKxls" },
                    { new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5"), null, "Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time raporting, printing legitimations and many others.", "Aplikacja Asp.NET MVC 5 silnie powiązana z systemem SAP. Ma wiele funkcjonalności jak na przykład: grafikowanie, raportowanie czasu pracy, drukowanie legitymacji i wiele innych. ", 0, "Development/Design", "Programowanie/Projektowanie", "Modular web application designed to manage various corporation processes.", "Modularna aplikacja webowa służąca do zarządzania wieloma różnymi procesami w korporacji.", new DateTime(2015, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "i360" },
                    { new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"), new DateTime(2018, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time raporting, printing legitimations and many others.", "Aplikacja Asp.NET MVC 5 silnie powiązana z systemem SAP. Ma wiele funkcjonalności jak na przykład: grafikowanie, raportowanie czasu pracy, drukowanie legitymacji i wiele innych. ", 0, "Development/Design", "Programowanie/Projektowanie", "Modular web application designed to manage various corporation processes.", "Modularna aplikacja webowa służąca do zarządzania wieloma różnymi procesami w korporacji.", new DateTime(2017, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rodo Manager" },
                    { new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068"), new DateTime(2019, 2, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asp.NET CORE with Angular application in CQRS concept. Connected with SAP ERP to import data by WCF. It uses SignalR or WebAPI to communicate. It uses Ocelote gateway to load balancing and rerouting.", "Aplikacja Asp.NET CORE z frontendem w Angularze napisana w koncepcji CQRS. Połączona z systemem SAP. Do komunikacji używane jest RESTowe WebAPI lub SignalR. Ocelote gateway używany jest do load balancingu i reroutingu.", 0, "Development/Design", "Programowanie/Projektowanie", "Incident raporting web application. Created in microservices architecture.", "Aplikacja webowa do raportowania incydentów na obiektach. Napisana w architekturze mikroserwisów.", new DateTime(2018, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ManagerFM" },
                    { new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"), new DateTime(2018, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asp.NET CORE 1.X with Razor/jQuery/Bootstrap4. Fun project which was kind of fantasy league during FIFA WorldCup 2018. A lot of statistics, mobile friendly.", "Aplikacja Asp.NET CORE 1.X z frontendem w Razorze/jQuery/Bootstrap4. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.", 0, "Idea/Development/Design", "Pomysł/Programowanie/Projektowanie", "Asp.NET CORE 1.X application. Fun project which was kind of fantasy league during FIFA WorldCup 2018", "Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018.", new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "BetWorldCup" },
                    { new Guid("35847dea-16ef-40dc-8769-96c0aae016e5"), new DateTime(2016, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Asp.NET MVC 5 with Razor/jQuery/Bootstrap3. Fun project which was kind of fantasy league during UEFA Euro 2016. A lot of statistics, mobile friendly.", "Aplikacja Asp.NET MVC 5 z frontendem w Razorze/jQuery/Bootstrap3. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Europy 2016. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.", 0, "Idea/Development/Design", "Pomysł/Programowanie/Projektowanie", "Asp.NET MVC 5 application. Fun project which was kind of fantasy league during UEFA Euro 2016", "Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Europy 2016.", new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "BetEuro" }
                });

            migrationBuilder.InsertData(
                table: "Skills",
                columns: new[] { "Id", "EnglishText", "Percent", "PolishText" },
                values: new object[,]
                {
                    { 8, "MS SQL", 55, "MS SQL" },
                    { 7, "Teamwork", 80, "Praca zespołowa" },
                    { 6, "Design", 75, "Projektowanie" },
                    { 5, "Mentoring", 70, "Mentorowanie" },
                    { 2, "Front-end", 30, "Front-end" },
                    { 3, "Web", 90, "Web" },
                    { 1, "Back-end", 90, "Back-end" },
                    { 4, "Desktop", 40, "Desktop" }
                });

            migrationBuilder.InsertData(
                table: "TimelineEvents",
                columns: new[] { "Id", "EndYear", "EnglishText", "PolishText", "StartYear" },
                values: new object[,]
                {
                    { 3, 2015, "IBM GSDC Poland|Software Packager", "IBM GSDC Polska|Software Packager", 2014 },
                    { 1, null, "Bachelor in Computer Science", "Uzyskanie tytułu inżyniera informatyki", 2014 },
                    { 2, 2014, "SoftCOM|.NET developer", "SoftCOM|Programista .NET", 2013 },
                    { 4, null, "SI-Consulting Sp. z o.o.|.NET developer|.NET team leader", "SI-Consulting Sp. z o.o.|Programista .NET|Lider zespołu .NET", 2015 }
                });

            migrationBuilder.InsertData(
                table: "BackendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 13, "C#", "C#", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 29, "C#", "C#", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 16, "ASP.NET MVC5", "ASP.NET MVC5", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 1, "WPF", "WPF", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 2, "Azure Blob", "Azure Blob", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 12, "SignalR", "SignalR", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 11, "Web Sockets", "Web Sockets", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 10, "REST", "REST", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 9, "WCF", "WCF", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 8, "MS SQL EF Core Code-First", "MS SQL EF Core Code-First", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 7, "Console Application", "Console Application", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 30, "ASP.NET CORE 1.X", "ASP.NET CORE 1.X", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 6, "ASP.NET CORE 2.X", "ASP.NET CORE 2.X", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 22, "C#", "C#", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 23, "ASP.NET MVC5", "ASP.NET MVC5", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 24, "MS SQL EF Code-First", "MS SQL EF Code-First", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 25, "WCF", "WCF", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 26, "REST", "REST", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 27, "ODATA", "ODATA", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 28, "RFC", "RFC", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 19, "WCF", "WCF", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 18, "MS SQL EF Code-First", "MS SQL EF Code-First", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 17, "Console Application", "Console Application", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 5, "C#", "C#", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 31, "MS SQL EF Core Code-First", "MS SQL EF Core Code-First", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 38, "C#", "C#", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 33, "Web Sockets", "Web Sockets", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 14, "WinForms", "WinForms", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 3, "C#", "C#", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 4, "WinForms", "WinForms", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 32, "REST", "REST", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 20, "C#", "C#", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 34, "C#", "C#", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 35, "ASP.NET WebForms", "ASP.NET WebForms", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 36, "Console Application", "Console Application", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 37, "MS SQL EF Model-First", "MS SQL EF Model-First", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 21, "ASP.NET MVC5", "ASP.NET MVC5", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 15, "C#", "C#", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") }
                });

            migrationBuilder.InsertData(
                table: "FrontendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 30, "Bootstrap 4.X", "Bootstrap 4.X", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 21, "JavaScript", "JavaScript", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 20, "jQuery", "jQuery", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 8, "Kendo UI", "Kendo UI", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 9, "Bootstrap 3.X", "Bootstrap 3.X", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 10, "Razor", "Razor", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 11, "HTML", "HTML", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 12, "CSS", "CSS", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 13, "Bootstrap theme", "Bootstrap theme", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 15, "JavaScript", "JavaScript", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 34, "Bootstrap theme", "Bootstrap theme", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 19, "CSS", "CSS", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 17, "Razor", "Razor", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 6, "TypeScript", "TypeScript", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 5, "CSS", "CSS", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 16, "Bootstrap 3.X", "Bootstrap 3.X", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 31, "Razor", "Razor", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 32, "HTML", "HTML", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 36, "JavaScript", "JavaScript", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 35, "jQuery", "jQuery", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 33, "CSS", "CSS", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 4, "HTML", "HTML", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 18, "HTML", "HTML", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 3, "Angular 7", "Angular 7", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 14, "jQuery", "jQuery", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 29, "JavaScript", "JavaScript", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 26, "CSS", "CSS", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 27, "Bootstrap theme", "Bootstrap theme", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 28, "jQuery", "jQuery", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 23, "Bootstrap 3.X", "Bootstrap 3.X", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 40, "JavaScript", "JavaScript", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 39, "CSS", "CSS", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 38, "HTML", "HTML", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 37, "Telerik ASP.NET AJAX Controls", "Telerik ASP.NET AJAX Controls", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 25, "HTML", "HTML", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 22, "Kendo UI", "Kendo UI", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 2, "Default WinForms Controls", "Default WinForms Controls", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 7, "Default WinForms Controls", "Default WinForms Controls", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 41, "XAML", "XAML", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 1, "Metro UI", "Metro UI", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 24, "Razor", "Razor", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") }
                });

            migrationBuilder.InsertData(
                table: "Libraries",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 23, "Piwik", "Piwik", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 5, "DI (Microsoft)", "DI (Microsoft)", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 4, "Automapper", "Automapper", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 8, "Serilog", "Serilog", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 30, "Xades", "Xades", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 7, "Swagger", "Swagger", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 24, "Hotjar", "Hotjar", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 9, "OpenCQRS", "OpenCQRS", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 26, "Automapper", "Automapper", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 27, "DI (Microsoft)", "DI (Microsoft)", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 28, "Google Anaytics", "Google Anaytics", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 29, "Serilog", "Serilog", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 3, "MSI Customization Libraries", "MSI Customization Libraries", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 16, "DI (Ninject)", "DI (Ninject)", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 17, "Google Anaytics", "Google Anaytics", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 18, "Serilog", "Serilog", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 1, "PDF Generator", "PDF Generator", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 2, "Wix Toolset", "Wix Toolset", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 6, "Ocelot", "Ocelot", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 11, "PDF Generator", "PDF Generator", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 22, "SAPNco Utils", "SAPNco Utils", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 19, "Automapper", "Automapper", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 10, "Screen To PDF", "Screen To PDF", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 20, "DI (Ninject)", "DI (Ninject)", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 12, "Automapper", "Automapper", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 13, "Google Anaytics", "Google Anaytics", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 14, "Piwik", "Piwik", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 15, "Hotjar", "Hotjar", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 21, "Google Anaytics", "Google Anaytics", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 25, "Serilog", "Serilog", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") }
                });

            migrationBuilder.InsertData(
                table: "Others",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 18, "OOP", "OOP", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 36, "JSON", "JSON", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 48, "Team Project", "Projekt grupowy", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 7, "Individual Project", "Projekt indywidualny", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 6, "XML", "XML", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 47, "XML", "XML", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 46, "JSON", "JSON", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 45, "ERP Data Import", "ERP Data Import", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 44, "Solid", "Solid", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 49, "Git", "Git", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 50, "Bitbucket", "Bitbucket", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 51, "Trello", "Trello", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 52, "OOP", "OOP", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 53, "Solid", "Solid", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 54, "JSON", "JSON", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 55, "XML", "XML", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 56, "Team Project", "Projekt grupowy", new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94") },
                    { 35, "OOP", "OOP", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 5, "OOP", "OOP", new Guid("6e5000e8-2305-4945-b8c7-4119f204b492") },
                    { 34, "Trello", "Trello", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 33, "Bitbucket", "Bitbucket", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 32, "Git", "Git", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 43, "OOP", "OOP", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 42, "Phabricator", "Phabricator", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 39, "Git", "Git", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 40, "Agile", "Agile", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 21, "Individual Project", "Projekt indywidualny", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 20, "SVG", "SVG", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 19, "XML", "XML", new Guid("a9fa3928-8d30-427d-a839-6a71f445884a") },
                    { 41, "TeamCity", "TeamCity", new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5") },
                    { 26, "Phabricator", "Phabricator", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 59, "Phabricator", "Phabricator", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 58, "Git", "Git", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 27, "OOP", "OOP", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 28, "Solid", "Solid", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 29, "JSON", "JSON", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 30, "XML", "XML", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 31, "Team Project", "Projekt grupowy", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 4, "Individual Project", "Projekt indywidualny", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 3, "XML", "XML", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 2, "OOP", "OOP", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 1, "Phabricator", "Phabricator", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 65, "SVN", "SVN", new Guid("eb294000-d308-412a-a79b-2b27b987ae57") },
                    { 24, "Agile", "Agile", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 23, "Git", "Git", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 22, "SVN", "SVN", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 64, "Individual Project", "Projekt indywidualny", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 57, "SVN", "SVN", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 63, "XML", "XML", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 8, "Git", "Git", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 9, "Bitbucket", "Bitbucket", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 10, "Agile", "Agile", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 11, "TeamCity", "TeamCity", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 12, "Jira", "Jira", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 13, "OOP", "OOP", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 14, "Solid", "Solid", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 15, "ERP Data Import", "ERP Data Import", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 16, "JSON", "JSON", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 17, "Team Project", "Projekt grupowy", new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068") },
                    { 62, "AD Users", "AD Users", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 61, "Solid", "Solid", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 60, "OOP", "OOP", new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4") },
                    { 25, "TeamCity", "TeamCity", new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2") },
                    { 37, "XML", "XML", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") },
                    { 38, "Individual Project", "Projekt indywidualny", new Guid("35847dea-16ef-40dc-8769-96c0aae016e5") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BackendTechnologies_ProjectId",
                table: "BackendTechnologies",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_FrontendTechnologies_ProjectId",
                table: "FrontendTechnologies",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Libraries_ProjectId",
                table: "Libraries",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Others_ProjectId",
                table: "Others",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BackendTechnologies");

            migrationBuilder.DropTable(
                name: "FrontendTechnologies");

            migrationBuilder.DropTable(
                name: "Libraries");

            migrationBuilder.DropTable(
                name: "Others");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "TimelineEvents");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
