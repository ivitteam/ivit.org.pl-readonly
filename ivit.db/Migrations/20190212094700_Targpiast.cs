﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class Targpiast : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"),
                columns: new[] { "LongDescription", "LongDescriptionPl", "ShortDescriptionPl", "Title" },
                values: new object[] { "Asp.NET MVC 5 application with MS SQL database. RODOmanager is application, which will help you manage your General Data Protection Regulation rules.", "Aplikacja Asp.NET MVC 5 z bazą danych w MS SQL. RODOmanager to aplikacja, która pomoże Ci samodzielnie sprawdzić i uporządkować dane osobowe w firmie.", "RODOmanager to aplikacja, która pomoże Ci samodzielnie sprawdzić i uporządkować dane osobowe w firmie", "RODOManager" });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "EndDate", "LongDescription", "LongDescriptionPl", "ProjectType", "Role", "RolePl", "ShortDescription", "ShortDescriptionPl", "StartDate", "Title" },
                values: new object[] { new Guid("b72baa1e-0512-437f-a902-28968d84e6fa"), new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "ASP.NET MVC 5 application with MS SQL Database. It is official website for biggest agri-food square in Lower Silesia. This application contains several CMS’s and administrator panel with many statistics.", "Aplikacja Asp.NET MVC 5 z bazą danych MS SQL. Jest to oficjalna strona internetowa największego na dolnym śląsku rynku rolno-spożywczego. Aplikacja posiada kilka CMS'ów oraz panel administracyjny z dużą ilością statystyk.", 0, "Development/Design", "Programowanie/Projektowanie", "Official website of biggest agri-food square in Lower Silesia.", "Oficjalna strona internetowa największego rynku rolno-spożywczego na dolnym śląsku.", new DateTime(2017, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Targpiast" });

            migrationBuilder.InsertData(
                table: "BackendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 39, "C#", "C#", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 40, "ASP.NET MVC5", "ASP.NET MVC5", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 41, "MS SQL EF Code-First", "MS SQL EF Code-First", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") }
                });

            migrationBuilder.InsertData(
                table: "FrontendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 42, "HTML", "HTML", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 43, "CSS", "CSS", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 44, "JavaScript", "JavaScript", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 45, "Bootstrap theme", "Bootstrap theme", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 46, "jQuery", "jQuery", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 47, "Razor", "Razor", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 48, "Bootstrap 3.X", "Bootstrap 3.X", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") }
                });

            migrationBuilder.InsertData(
                table: "Libraries",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 35, "Serilog", "Serilog", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 34, "Google Anaytics", "Google Anaytics", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 32, "Automapper", "Automapper", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 31, "Serilog", "Serilog", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 33, "DI (Ninject)", "DI (Microsoft)", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") }
                });

            migrationBuilder.InsertData(
                table: "Others",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 72, "JSON", "JSON", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 66, "Team Project", "Projekt grupowy", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 67, "Git", "Git", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 68, "Bitbucket", "Bitbucket", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 69, "Trello", "Trello", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 70, "OOP", "OOP", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 71, "Solid", "Solid", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") },
                    { 73, "XML", "XML", new Guid("b72baa1e-0512-437f-a902-28968d84e6fa") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("b72baa1e-0512-437f-a902-28968d84e6fa"));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"),
                columns: new[] { "LongDescription", "LongDescriptionPl", "ShortDescriptionPl", "Title" },
                values: new object[] { "Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time raporting, printing legitimations and many others.", "Aplikacja Asp.NET MVC 5 silnie powiązana z systemem SAP. Ma wiele funkcjonalności jak na przykład: grafikowanie, raportowanie czasu pracy, drukowanie legitymacji i wiele innych. ", "Modularna aplikacja webowa służąca do zarządzania wieloma różnymi procesami w korporacji.", "Rodo Manager" });
        }
    }
}
