﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class EnglishSpellCheck : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"),
                column: "LongDescription",
                value: "WinForms application designed to automate part of the packaging process. It applies client best practices to loaded MSI file. It also has filesystem store of clients best practices. Additionally it creates start batch/vbs scripts.");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5"),
                column: "LongDescription",
                value: "Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time reporting, printing legitimations and many others.");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("eb294000-d308-412a-a79b-2b27b987ae57"),
                column: "LongDescription",
                value: "WPF desktop application. It is designed to generate JPK files from Excel templates. It also provides connectivity with MF gateway where you can send this files and receive UPO.");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"),
                column: "LongDescription",
                value: "WinForms application designed to automate part of the packaging process. It applies client best practices to loaded MSI file. It also has filesystem store of clients best practices. Additionaly it creates start batch/vbs scripts.");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5"),
                column: "LongDescription",
                value: "Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time raporting, printing legitimations and many others.");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("eb294000-d308-412a-a79b-2b27b987ae57"),
                column: "LongDescription",
                value: "WPF desktop application. It is designed to generate JPK files from Excel templates. It also provides connectivity with MF gateway where you can send this files and recieve UPO.");
        }
    }
}
