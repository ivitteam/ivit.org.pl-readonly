﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class ivit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 33,
                column: "DescriptionPl",
                value: "DI (Ninject)");

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "EndDate", "LongDescription", "LongDescriptionPl", "ProjectType", "Role", "RolePl", "ShortDescription", "ShortDescriptionPl", "StartDate", "Title" },
                values: new object[] { new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946"), new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "ASP.NET CORE 2.2, MS SQL, Razor Pages. This portfolio is a test page in Razor Pages frontend.", "ASP.NET CORE 2.2, MS SQL, Razor Pages. Utworzenie tego portfolio miało na celu przetestowanie frontendu napisanego w Razor Pages.", 0, "Idea/Development/Design", "Pomysł/Programowanie/Projektowanie", "My portfolio application.", "Moje portfolio.", new DateTime(2017, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ivit.org.pl" });

            migrationBuilder.InsertData(
                table: "BackendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 42, "C#", "C#", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 43, "ASP.NET CORE 2.X", "ASP.NET CORE 2.X", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 44, "MS SQL EF Core Code-First", "MS SQL EF Core Code-First", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") }
                });

            migrationBuilder.InsertData(
                table: "FrontendTechnologies",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 54, "Razor Pages", "Razor Pages", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 53, "jQuery", "jQuery", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 52, "Bootstrap theme", "Bootstrap theme", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 55, "Bootstrap 3.X", "Bootstrap 3.X", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 50, "CSS", "CSS", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 49, "HTML", "HTML", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 51, "JavaScript", "JavaScript", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") }
                });

            migrationBuilder.InsertData(
                table: "Libraries",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 36, "Automapper", "Automapper", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 37, "DI (Microsoft)", "DI (Microsoft)", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 38, "Google Anaytics", "Google Anaytics", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 39, "Serilog", "Serilog", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") }
                });

            migrationBuilder.InsertData(
                table: "Others",
                columns: new[] { "Id", "Description", "DescriptionPl", "ProjectId" },
                values: new object[,]
                {
                    { 78, "JSON", "JSON", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 74, "Git", "Git", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 75, "Bitbucket", "Bitbucket", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 76, "OOP", "OOP", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 77, "Solid", "Solid", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") },
                    { 79, "Individual Project", "Projekt indywidualny", new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "BackendTechnologies",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "FrontendTechnologies",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Others",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946"));

            migrationBuilder.UpdateData(
                table: "Libraries",
                keyColumn: "Id",
                keyValue: 33,
                column: "DescriptionPl",
                value: "DI (Microsoft)");
        }
    }
}
