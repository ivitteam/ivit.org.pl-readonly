﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class Bugfixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("35847dea-16ef-40dc-8769-96c0aae016e5"),
                column: "Title",
                value: "Bet Euro");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"),
                column: "Title",
                value: "Bet WorldCup");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"),
                column: "Title",
                value: "RODO Manager");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("35847dea-16ef-40dc-8769-96c0aae016e5"),
                column: "Title",
                value: "BetEuro");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"),
                column: "Title",
                value: "BetWorldCup");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"),
                column: "Title",
                value: "RODOManager");
        }
    }
}
