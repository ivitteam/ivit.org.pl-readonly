﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ivit.db.Migrations
{
    public partial class PolishSpellCheck : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"),
                column: "LongDescriptionPl",
                value: "Aplikacja służąca do wstępnej edycji plików MSI w procesie packagingu. Dodaje do nich dobre praktyki danego klienta. Dodatkowo posiada magazyn dobrych praktyk działający na systemie plików. Oprócz tego aplikacja tworzy pliki startowe (batch/vbs).");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"),
                column: "LongDescriptionPl",
                value: "Aplikacja służąca do wstepnej edycji plików MSI w procesie packagingu. Dodaje do nich dobre praktyki danego klienta. Dodatkowo posiada magazyn dobrych praktyk działający na systemie plików. Oprócz tego aplikacja tworzy pliki startowe (batch/vbs).");
        }
    }
}
