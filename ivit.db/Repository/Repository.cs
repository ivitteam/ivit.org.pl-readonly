﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ivit.db.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace ivit.db.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ApplicationDbContext ApplicationDbContext { get; }

        public Repository(ApplicationDbContext repositoryContext)
        {
            ApplicationDbContext = repositoryContext;
        }
 
        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await ApplicationDbContext.Set<T>().ToListAsync();
        }
 
        public async Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, bool>> expression)
        {
            return await ApplicationDbContext.Set<T>().Where(expression).ToListAsync();
        }
 
        public void Create(T entity)
        {
            ApplicationDbContext.Set<T>().Add(entity);
        }
 
        public void Update(T entity)
        {
            ApplicationDbContext.Set<T>().Update(entity);
        }
 
        public void Delete(T entity)
        {
            ApplicationDbContext.Set<T>().Remove(entity);
        }
 
        public async Task SaveAsync()
        {
            await ApplicationDbContext.SaveChangesAsync();
        }

        public IQueryable<T> GetSet()
        {
            return ApplicationDbContext.Set<T>().AsQueryable();
        }
    }
}
