﻿using System;
using ivit.db.Models;
using Microsoft.EntityFrameworkCore;

namespace ivit.db.Infrastructure
{
	public static class ModelBuilderExtensions
	{
		public static void SeedDatabase(this ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Skill>().HasData(
				new Skill
				{
					Id = 1,
					EnglishText = "Back-end",
					PolishText = "Back-end",
					Percent = 90
				},
				new Skill
				{
					Id = 2,
					EnglishText = "Front-end",
					PolishText = "Front-end",
					Percent = 30
				},
				new Skill
				{
					Id = 3,
					EnglishText = "Web",
					PolishText = "Web",
					Percent = 90
				},
				new Skill
				{
					Id = 4,
					EnglishText = "Desktop",
					PolishText = "Desktop",
					Percent = 40
				},
				new Skill
				{
					Id = 5,
					EnglishText = "Mentoring",
					PolishText = "Mentorowanie",
					Percent = 70
				},
				new Skill
				{
					Id = 6,
					EnglishText = "Design",
					PolishText = "Projektowanie",
					Percent = 75
				},
				new Skill
				{
					Id = 7,
					EnglishText = "Teamwork",
					PolishText = "Praca zespołowa",
					Percent = 80
				},
				new Skill
				{
					Id = 8,
					EnglishText = "MS SQL",
					PolishText = "MS SQL",
					Percent = 55
				}
			);

			modelBuilder.Entity<TimelineEvent>().HasData(
				new TimelineEvent
				{
					Id = 1,
					EnglishText = "Bachelor in Computer Science",
					PolishText = "Uzyskanie tytułu inżyniera informatyki",
					StartYear = 2014,
					EndYear = null
				},
				new TimelineEvent
				{
					Id = 2,
					EnglishText = "SoftCOM|.NET developer",
					PolishText = "SoftCOM|Programista .NET",
					StartYear = 2013,
					EndYear = 2014
				},
				new TimelineEvent
				{
					Id = 3,
					EnglishText = "IBM GSDC Poland|Software Packager",
					PolishText = "IBM GSDC Polska|Software Packager",
					StartYear = 2014,
					EndYear = 2015
				},
				new TimelineEvent
				{
					Id = 4,
					EnglishText = "SI-Consulting Sp. z o.o.|.NET developer|.NET team leader",
					PolishText = "SI-Consulting Sp. z o.o.|Programista .NET|Lider zespołu .NET",
					StartYear = 2015,
					EndYear = null
				}
			);

			modelBuilder.Entity<Project>().HasData(
				new Project
				{
					Id = new Guid("a9fa3928-8d30-427d-a839-6a71f445884a"),
					Title = "room Editor",
					ShortDescription = "Graphic editor for SVG files.",
					ShortDescriptionPl = "Edytor graficzny plików SVG.",
					LongDescription =
						"Graphic editor for SVG files. It's dedicated to drawing rooms with numbered seats which a later exported to ticket selling system. It uses WinForms technology with default UI. The most interesting part of this project is image to SVG parser.",
					LongDescriptionPl =
						"Edytor graficzny plików SVG. Przeznaczony do tworzenia wizualizacji sal z miejscami numerowanymi. Pliki importowane są do systemu biletowego. Aplikacja została napisana w WinForms, a jej najciekawszym elementem jest parser image -> SVG.",
					StartDate = new DateTime(2013, 9, 1),
					EndDate = new DateTime(2014, 2, 1),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Desktop
				},
				new Project
				{
					Id = new Guid("6e5000e8-2305-4945-b8c7-4119f204b492"),
					Title = "Software Packaging Tools",
					ShortDescription = "Automation tools for software packaging process.",
					ShortDescriptionPl = "Zestaw narzędzi automatyzujący część procesów przy paczkowaniu aplikacji.",
					LongDescription =
						"WinForms application designed to automate part of the packaging process. It applies client best practices to loaded MSI file. It also has filesystem store of clients best practices. Additionally it creates start batch/vbs scripts.",
					LongDescriptionPl =
						"Aplikacja służąca do wstępnej edycji plików MSI w procesie packagingu. Dodaje do nich dobre praktyki danego klienta. Dodatkowo posiada magazyn dobrych praktyk działający na systemie plików. Oprócz tego aplikacja tworzy pliki startowe (batch/vbs).",
					StartDate = new DateTime(2014, 6, 1),
					EndDate = new DateTime(2015, 5, 31),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Desktop
				},
				new Project
				{
					Id = new Guid("3cf50c9f-415d-494f-a6b4-ea5367d942e4"),
					Title = "Portal QV",
					ShortDescription = "Permissions managment for QlikView application.",
					ShortDescriptionPl = "Zarządzanie uprawnieniami do QlikView.",
					LongDescription =
						"Asp.NET WebForms application. It uses AD users, MS SQL database and file system data import to manage user permissions to raports generated in QlikView. ",
					LongDescriptionPl =
						"Aplikacja Asp.NET Web Forms używająca użytkowników z AD, bazy danych MS SQL i systemu plików do importu danych. Aplikacja służy do zarządzania uprawnieniami użytkowników do raportów generowanych w QlikView.",
					StartDate = new DateTime(2015, 6, 1),
					EndDate = new DateTime(2016, 5, 31),
					Role = "Maintenance/Development",
					RolePl = "Utrzymanie/Programowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("eb294000-d308-412a-a79b-2b27b987ae57"),
					Title = "Kreator JPKxls",
					ShortDescription = "JPK files generator and sender.",
					ShortDescriptionPl = "Program do wysyłki i genrowania plików JPK.",
					LongDescription =
						"WPF desktop application. It is designed to generate JPK files from Excel templates. It also provides connectivity with MF gateway where you can send this files and receive UPO.",
					LongDescriptionPl =
						"Aplikacja desktopowa WPF. Umożliwia generowanie plików JPK na podstawie wypełnionych szablonów z programu Excel. Dodatkowo gwarantuje połączenie z bramką ministerstwa finansów co pozwala wysłać te pliki i odebrać UPO.",
					StartDate = new DateTime(2015, 7, 1),
					EndDate = new DateTime(2016, 2, 1),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Desktop
				},
				new Project
				{
					Id = new Guid("79fa9802-8071-4fe9-a8c4-afd3a130dca5"),
					Title = "i360",
					ShortDescription = "Modular web application designed to manage various corporation processes.",
					ShortDescriptionPl =
						"Modularna aplikacja webowa służąca do zarządzania wieloma różnymi procesami w korporacji.",
					LongDescription =
						"Asp.NET MVC 5 application. Strongly connected with SAP ERP. It provides a lot of functionalities like scheduling, time reporting, printing legitimations and many others.",
					LongDescriptionPl =
						"Aplikacja Asp.NET MVC 5 silnie powiązana z systemem SAP. Ma wiele funkcjonalności jak na przykład: grafikowanie, raportowanie czasu pracy, drukowanie legitymacji i wiele innych. ",
					StartDate = new DateTime(2015, 10, 1),
					EndDate = null,
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("fbe95f72-b684-4ece-b1a1-81655cb654f2"),
					Title = "RODO Manager",
					ShortDescription = "Modular web application designed to manage various corporation processes.",
					ShortDescriptionPl =
						"RODOmanager to aplikacja, która pomoże Ci samodzielnie sprawdzić i uporządkować dane osobowe w firmie",
					LongDescription =
						"Asp.NET MVC 5 application with MS SQL database. RODOmanager is application, which will help you manage your General Data Protection Regulation rules.",
					LongDescriptionPl =
						"Aplikacja Asp.NET MVC 5 z bazą danych w MS SQL. RODOmanager to aplikacja, która pomoże Ci samodzielnie sprawdzić i uporządkować dane osobowe w firmie.",
					StartDate = new DateTime(2017, 04, 1),
					EndDate = new DateTime(2018, 10, 1),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("515d0eff-ff92-4eed-885f-63f9e0bff068"),
					Title = "ManagerFM",
					ShortDescription = "Incident raporting web application. Created in microservices architecture.",
					ShortDescriptionPl =
						"Aplikacja webowa do raportowania incydentów na obiektach. Napisana w architekturze mikroserwisów.",
					LongDescription =
						"Asp.NET CORE with Angular application in CQRS concept. Connected with SAP ERP to import data by WCF. It uses SignalR or WebAPI to communicate. It uses Ocelote gateway to load balancing and rerouting.",
					LongDescriptionPl =
						"Aplikacja Asp.NET CORE z frontendem w Angularze napisana w koncepcji CQRS. Połączona z systemem SAP. Do komunikacji używane jest RESTowe WebAPI lub SignalR. Ocelote gateway używany jest do load balancingu i reroutingu.",
					StartDate = new DateTime(2018, 09, 1),
					EndDate = new DateTime(2019, 02, 8),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("e1386160-a6c9-4ef4-9754-cff99c96ed94"),
					Title = "Bet WorldCup",
					ShortDescription =
						"Asp.NET CORE 2.X application. Fun project which was kind of fantasy league during FIFA WorldCup 2018",
					ShortDescriptionPl =
						"Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018.",
					LongDescription =
						"Asp.NET CORE 2.X with Razor/jQuery/Bootstrap4. Fun project which was kind of fantasy league during FIFA WorldCup 2018. A lot of statistics, mobile friendly.",
					LongDescriptionPl =
						"Aplikacja Asp.NET CORE 2.X z frontendem w Razorze/jQuery/Bootstrap4. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Świata 2018. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.",
					StartDate = new DateTime(2018, 01, 1),
					EndDate = new DateTime(2018, 07, 20),
					Role = "Idea/Development/Design",
					RolePl = "Pomysł/Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("35847dea-16ef-40dc-8769-96c0aae016e5"),
					Title = "Bet Euro",
					ShortDescription =
						"Asp.NET MVC 5 application. Fun project which was kind of fantasy league during UEFA Euro 2016",
					ShortDescriptionPl =
						"Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Europy 2016.",
					LongDescription =
						"Asp.NET MVC 5 with Razor/jQuery/Bootstrap3. Fun project which was kind of fantasy league during UEFA Euro 2016. A lot of statistics, mobile friendly.",
					LongDescriptionPl =
						"Aplikacja Asp.NET MVC 5 z frontendem w Razorze/jQuery/Bootstrap3. Aplikacja webowa do obstawiania wyników meczów podczas Mistrzostw Europy 2016. Dużo statystyk, aplikacja przystosowana do urządzeń mobilnych.",
					StartDate = new DateTime(2016, 01, 1),
					EndDate = new DateTime(2016, 07, 20),
					Role = "Idea/Development/Design",
					RolePl = "Pomysł/Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa"),
					Title = "Targpiast",
					ShortDescription =
						"Official website of biggest agri-food square in Lower Silesia.",
					ShortDescriptionPl =
						"Oficjalna strona internetowa największego rynku rolno-spożywczego na dolnym śląsku.",
					LongDescription =
						"ASP.NET MVC 5 application with MS SQL Database. It is official website for biggest agri-food square in Lower Silesia. This application contains several CMS’s and administrator panel with many statistics.",
					LongDescriptionPl =
						"Aplikacja Asp.NET MVC 5 z bazą danych MS SQL. Jest to oficjalna strona internetowa największego na dolnym śląsku rynku rolno-spożywczego. Aplikacja posiada kilka CMS'ów oraz panel administracyjny z dużą ilością statystyk.",
					StartDate = new DateTime(2017, 11, 1),
					EndDate = new DateTime(2018, 06, 15),
					Role = "Development/Design",
					RolePl = "Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				},
				new Project
				{
					Id = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946"),
					Title = "ivit.org.pl",
					ShortDescription =
						"My portfolio application.",
					ShortDescriptionPl =
						"Moje portfolio.",
					LongDescription =
						"ASP.NET CORE 2.2, MS SQL, Razor Pages. This portfolio is a test page in Razor Pages frontend.",
					LongDescriptionPl =
						"ASP.NET CORE 2.2, MS SQL, Razor Pages. Utworzenie tego portfolio miało na celu przetestowanie frontendu napisanego w Razor Pages.",
					StartDate = new DateTime(2018, 12, 1),
					EndDate = new DateTime(2019, 02, 12),
					Role = "Idea/Development/Design",
					RolePl = "Pomysł/Programowanie/Projektowanie",
					ProjectType = ProjectTypeEnum.Web
				}
			);

			modelBuilder.Entity<Library>()
				.HasData(
					new Library
					{
						Id = 1, Description = "PDF Generator", DescriptionPl = "PDF Generator",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Library
					{
						Id = 2, Description = "Wix Toolset", DescriptionPl = "Wix Toolset",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Library
					{
						Id = 3, Description = "MSI Customization Libraries",
						DescriptionPl = "MSI Customization Libraries",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new Library
					{
						Id = 4, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 5, Description = "DI (Microsoft)", DescriptionPl = "DI (Microsoft)",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 6, Description = "Ocelot", DescriptionPl = "Ocelot",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 7, Description = "Swagger", DescriptionPl = "Swagger",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 8, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 9, Description = "OpenCQRS", DescriptionPl = "OpenCQRS",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Library
					{
						Id = 10, Description = "Screen To PDF", DescriptionPl = "Screen To PDF",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 11, Description = "PDF Generator", DescriptionPl = "PDF Generator",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 12, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 13, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 14, Description = "Piwik", DescriptionPl = "Piwik",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 15, Description = "Hotjar", DescriptionPl = "Hotjar",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Library
					{
						Id = 16, Description = "DI (Ninject)", DescriptionPl = "DI (Ninject)",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Library
					{
						Id = 17, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Library
					{
						Id = 18, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Library
					{
						Id = 19, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 20, Description = "DI (Ninject)", DescriptionPl = "DI (Ninject)",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 21, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 22, Description = "SAPNco Utils", DescriptionPl = "SAPNco Utils",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 23, Description = "Piwik", DescriptionPl = "Piwik",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 24, Description = "Hotjar", DescriptionPl = "Hotjar",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 25, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Library
					{
						Id = 26, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Library
					{
						Id = 27, Description = "DI (Microsoft)", DescriptionPl = "DI (Microsoft)",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Library
					{
						Id = 28, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Library
					{
						Id = 29, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Library
					{
						Id = 30, Description = "Xades", DescriptionPl = "Xades",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Library
					{
						Id = 31, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Library
					{
						Id = 32, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Library
					{
						Id = 33, Description = "DI (Ninject)", DescriptionPl = "DI (Ninject)",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Library
					{
						Id = 34, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Library
					{
						Id = 35, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Library
					{
						Id = 36, Description = "Automapper", DescriptionPl = "Automapper",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Library
					{
						Id = 37, Description = "DI (Microsoft)", DescriptionPl = "DI (Microsoft)",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Library
					{
						Id = 38, Description = "Google Anaytics", DescriptionPl = "Google Anaytics",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Library
					{
						Id = 39, Description = "Serilog", DescriptionPl = "Serilog",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					}
				);

			modelBuilder.Entity<FrontendTechnology>()
				.HasData(
					new FrontendTechnology
					{
						Id = 1, Description = "Metro UI", DescriptionPl = "Metro UI",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new FrontendTechnology
					{
						Id = 2, Description = "Default WinForms Controls", DescriptionPl = "Default WinForms Controls",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new FrontendTechnology
					{
						Id = 3, Description = "Angular 7", DescriptionPl = "Angular 7",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new FrontendTechnology
					{
						Id = 4, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new FrontendTechnology
					{
						Id = 5, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new FrontendTechnology
					{
						Id = 6, Description = "TypeScript", DescriptionPl = "TypeScript",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new FrontendTechnology
					{
						Id = 7, Description = "Default WinForms Controls", DescriptionPl = "Default WinForms Controls",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new FrontendTechnology
					{
						Id = 8, Description = "Kendo UI", DescriptionPl = "Kendo UI",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 9, Description = "Bootstrap 3.X", DescriptionPl = "Bootstrap 3.X",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 10, Description = "Razor", DescriptionPl = "Razor",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 11, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 12, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 13, Description = "Bootstrap theme", DescriptionPl = "Bootstrap theme",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 14, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 15, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new FrontendTechnology
					{
						Id = 16, Description = "Bootstrap 3.X", DescriptionPl = "Bootstrap 3.X",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 17, Description = "Razor", DescriptionPl = "Razor",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 18, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 19, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 20, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 21, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new FrontendTechnology
					{
						Id = 22, Description = "Kendo UI", DescriptionPl = "Kendo UI",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 23, Description = "Bootstrap 3.X", DescriptionPl = "Bootstrap 3.X",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 24, Description = "Razor", DescriptionPl = "Razor",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 25, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 26, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 27, Description = "Bootstrap theme", DescriptionPl = "Bootstrap theme",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 28, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 29, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new FrontendTechnology
					{
						Id = 30, Description = "Bootstrap 4.X", DescriptionPl = "Bootstrap 4.X",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 31, Description = "Razor", DescriptionPl = "Razor",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 32, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 33, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 34, Description = "Bootstrap theme", DescriptionPl = "Bootstrap theme",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 35, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 36, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new FrontendTechnology
					{
						Id = 37, Description = "Telerik ASP.NET AJAX Controls",
						DescriptionPl = "Telerik ASP.NET AJAX Controls",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new FrontendTechnology
					{
						Id = 38, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new FrontendTechnology
					{
						Id = 39, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new FrontendTechnology
					{
						Id = 40, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new FrontendTechnology
					{
						Id = 41, Description = "XAML", DescriptionPl = "XAML",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new FrontendTechnology
					{
						Id = 42, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 43, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 44, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 45, Description = "Bootstrap theme", DescriptionPl = "Bootstrap theme",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 46, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 47, Description = "Razor", DescriptionPl = "Razor",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 48, Description = "Bootstrap 3.X", DescriptionPl = "Bootstrap 3.X",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new FrontendTechnology
					{
						Id = 49, Description = "HTML", DescriptionPl = "HTML",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 50, Description = "CSS", DescriptionPl = "CSS",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 51, Description = "JavaScript", DescriptionPl = "JavaScript",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 52, Description = "Bootstrap theme", DescriptionPl = "Bootstrap theme",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 53, Description = "jQuery", DescriptionPl = "jQuery",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 54, Description = "Razor Pages", DescriptionPl = "Razor Pages",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new FrontendTechnology
					{
						Id = 55, Description = "Bootstrap 3.X", DescriptionPl = "Bootstrap 3.X",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					}
				);

			modelBuilder.Entity<Other>()
				.HasData(
					new Other
					{
						Id = 1, Description = "Phabricator", DescriptionPl = "Phabricator",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Other
					{
						Id = 2, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Other
					{
						Id = 3, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Other
					{
						Id = 4, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Other
					{
						Id = 5, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new Other
					{
						Id = 6, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new Other
					{
						Id = 7, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new Other
					{
						Id = 8, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 9, Description = "Bitbucket", DescriptionPl = "Bitbucket",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 10, Description = "Agile", DescriptionPl = "Agile",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 11, Description = "TeamCity", DescriptionPl = "TeamCity",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 12, Description = "Jira", DescriptionPl = "Jira",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 13, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 14, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 15, Description = "ERP Data Import", DescriptionPl = "ERP Data Import",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 16, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 17, Description = "Team Project", DescriptionPl = "Projekt grupowy",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new Other
					{
						Id = 18, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new Other
					{
						Id = 19, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new Other
					{
						Id = 20, Description = "SVG", DescriptionPl = "SVG",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new Other
					{
						Id = 21, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new Other
					{
						Id = 22, Description = "SVN", DescriptionPl = "SVN",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 23, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 24, Description = "Agile", DescriptionPl = "Agile",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 25, Description = "TeamCity", DescriptionPl = "TeamCity",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 26, Description = "Phabricator", DescriptionPl = "Phabricator",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 27, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 28, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 29, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 30, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 31, Description = "Team Project", DescriptionPl = "Projekt grupowy",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new Other
					{
						Id = 32, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 33, Description = "Bitbucket", DescriptionPl = "Bitbucket",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 34, Description = "Trello", DescriptionPl = "Trello",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 35, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 36, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 37, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 38, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new Other
					{
						Id = 39, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 40, Description = "Agile", DescriptionPl = "Agile",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 41, Description = "TeamCity", DescriptionPl = "TeamCity",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 42, Description = "Phabricator", DescriptionPl = "Phabricator",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 43, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 44, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 45, Description = "ERP Data Import", DescriptionPl = "ERP Data Import",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 46, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 47, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 48, Description = "Team Project", DescriptionPl = "Projekt grupowy",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new Other
					{
						Id = 49, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 50, Description = "Bitbucket", DescriptionPl = "Bitbucket",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 51, Description = "Trello", DescriptionPl = "Trello",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 52, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 53, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 54, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 55, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 56, Description = "Team Project", DescriptionPl = "Projekt grupowy",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new Other
					{
						Id = 57, Description = "SVN", DescriptionPl = "SVN",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 58, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 59, Description = "Phabricator", DescriptionPl = "Phabricator",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 60, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 61, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 62, Description = "AD Users", DescriptionPl = "AD Users",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 63, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 64, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new Other
					{
						Id = 65, Description = "SVN", DescriptionPl = "SVN",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new Other
					{
						Id = 66, Description = "Team Project", DescriptionPl = "Projekt grupowy",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 67, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 68, Description = "Bitbucket", DescriptionPl = "Bitbucket",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 69, Description = "Trello", DescriptionPl = "Trello",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 70, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 71, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 72, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 73, Description = "XML", DescriptionPl = "XML",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new Other
					{
						Id = 74, Description = "Git", DescriptionPl = "Git",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Other
					{
						Id = 75, Description = "Bitbucket", DescriptionPl = "Bitbucket",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Other
					{
						Id = 76, Description = "OOP", DescriptionPl = "OOP",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Other
					{
						Id = 77, Description = "Solid", DescriptionPl = "Solid",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Other
					{
						Id = 78, Description = "JSON", DescriptionPl = "JSON",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new Other
					{
						Id = 79, Description = "Individual Project", DescriptionPl = "Projekt indywidualny",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					}
				);

			modelBuilder.Entity<BackendTechnology>()
				.HasData(
					new BackendTechnology
					{
						Id = 1, Description = "WPF", DescriptionPl = "WPF",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new BackendTechnology
					{
						Id = 2, Description = "Azure Blob", DescriptionPl = "Azure Blob",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new BackendTechnology
					{
						Id = 3, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new BackendTechnology
					{
						Id = 4, Description = "WinForms", DescriptionPl = "WinForms",
						ProjectId = new Guid("6E5000E8-2305-4945-B8C7-4119F204B492")
					},
					new BackendTechnology
					{
						Id = 5, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 6, Description = "ASP.NET CORE 2.X", DescriptionPl = "ASP.NET CORE 2.X",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 7, Description = "Console Application", DescriptionPl = "Console Application",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 8, Description = "MS SQL EF Core Code-First", DescriptionPl = "MS SQL EF Core Code-First",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 9, Description = "WCF", DescriptionPl = "WCF",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 10, Description = "REST", DescriptionPl = "REST",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 11, Description = "Web Sockets", DescriptionPl = "Web Sockets",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 12, Description = "SignalR", DescriptionPl = "SignalR",
						ProjectId = new Guid("515D0EFF-FF92-4EED-885F-63F9E0BFF068")
					},
					new BackendTechnology
					{
						Id = 13, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new BackendTechnology
					{
						Id = 14, Description = "WinForms", DescriptionPl = "WinForms",
						ProjectId = new Guid("A9FA3928-8D30-427D-A839-6A71F445884A")
					},
					new BackendTechnology
					{
						Id = 15, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new BackendTechnology
					{
						Id = 16, Description = "ASP.NET MVC5", DescriptionPl = "ASP.NET MVC5",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new BackendTechnology
					{
						Id = 17, Description = "Console Application", DescriptionPl = "Console Application",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new BackendTechnology
					{
						Id = 18, Description = "MS SQL EF Code-First", DescriptionPl = "MS SQL EF Code-First",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new BackendTechnology
					{
						Id = 19, Description = "WCF", DescriptionPl = "WCF",
						ProjectId = new Guid("FBE95F72-B684-4ECE-B1A1-81655CB654F2")
					},
					new BackendTechnology
					{
						Id = 20, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new BackendTechnology
					{
						Id = 21, Description = "ASP.NET MVC5", DescriptionPl = "ASP.NET MVC5",
						ProjectId = new Guid("35847DEA-16EF-40DC-8769-96C0AAE016E5")
					},
					new BackendTechnology
					{
						Id = 22, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 23, Description = "ASP.NET MVC5", DescriptionPl = "ASP.NET MVC5",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 24, Description = "MS SQL EF Code-First", DescriptionPl = "MS SQL EF Code-First",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 25, Description = "WCF", DescriptionPl = "WCF",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 26, Description = "REST", DescriptionPl = "REST",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 27, Description = "ODATA", DescriptionPl = "ODATA",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 28, Description = "RFC", DescriptionPl = "RFC",
						ProjectId = new Guid("79FA9802-8071-4FE9-A8C4-AFD3A130DCA5")
					},
					new BackendTechnology
					{
						Id = 29, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new BackendTechnology
					{
						Id = 30, Description = "ASP.NET CORE 2.X", DescriptionPl = "ASP.NET CORE 2.X",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new BackendTechnology
					{
						Id = 31, Description = "MS SQL EF Core Code-First", DescriptionPl = "MS SQL EF Core Code-First",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new BackendTechnology
					{
						Id = 32, Description = "REST", DescriptionPl = "REST",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new BackendTechnology
					{
						Id = 33, Description = "Web Sockets", DescriptionPl = "Web Sockets",
						ProjectId = new Guid("E1386160-A6C9-4EF4-9754-CFF99C96ED94")
					},
					new BackendTechnology
					{
						Id = 34, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new BackendTechnology
					{
						Id = 35, Description = "ASP.NET WebForms", DescriptionPl = "ASP.NET WebForms",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new BackendTechnology
					{
						Id = 36, Description = "Console Application", DescriptionPl = "Console Application",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new BackendTechnology
					{
						Id = 37, Description = "MS SQL EF Model-First", DescriptionPl = "MS SQL EF Model-First",
						ProjectId = new Guid("3CF50C9F-415D-494F-A6B4-EA5367D942E4")
					},
					new BackendTechnology
					{
						Id = 38, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("EB294000-D308-412A-A79B-2B27B987AE57")
					},
					new BackendTechnology
					{
						Id = 39, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new BackendTechnology
					{
						Id = 40, Description = "ASP.NET MVC5", DescriptionPl = "ASP.NET MVC5",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new BackendTechnology
					{
						Id = 41, Description = "MS SQL EF Code-First", DescriptionPl = "MS SQL EF Code-First",
						ProjectId = new Guid("b72baa1e-0512-437f-a902-28968d84e6fa")
					},
					new BackendTechnology
					{
						Id = 42, Description = "C#", DescriptionPl = "C#",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new BackendTechnology
					{
						Id = 43, Description = "ASP.NET CORE 2.X", DescriptionPl = "ASP.NET CORE 2.X",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					},
					new BackendTechnology
					{
						Id = 44, Description = "MS SQL EF Core Code-First", DescriptionPl = "MS SQL EF Core Code-First",
						ProjectId = new Guid("4175bb2c-08e7-4c05-9ae1-92365a37c946")
					}
				);
		}
	}
}