﻿using ivit.db.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
// ReSharper disable All

namespace ivit.db.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        private DbSet<TimelineEvent> TimelineEvents { get; set; }
        private DbSet<Skill> Skills { get; set; }
        private DbSet<Project> Projects { get; set; }
        private DbSet<BackendTechnology> BackendTechnologies { get; set; }
        private DbSet<FrontendTechnology> FrontendTechnologies { get; set; }
        private DbSet<Library> Libraries { get; set; }
        private DbSet<Other> Others { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.SeedDatabase();
        }
    }
}
