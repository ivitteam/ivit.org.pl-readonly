﻿namespace ivit.db.Models
{
    public class TimelineEvent
    {
        public int Id { get; set; }
        public int StartYear { get; set; }
        public int? EndYear { get; set; }
        public string EnglishText { get; set; }
        public string PolishText { get; set; }
    }
}
