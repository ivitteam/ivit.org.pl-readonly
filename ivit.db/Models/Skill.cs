﻿namespace ivit.db.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public int Percent { get; set; }
        public string EnglishText { get; set; }
        public string PolishText { get; set; }
    }
}
