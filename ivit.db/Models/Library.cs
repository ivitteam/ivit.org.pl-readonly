﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ivit.db.Models
{
    public class Library
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id{ get; set; }
        public string Description { get; set; }
        public string DescriptionPl { get; set; }
        public Guid ProjectId { get; set; }
    }
}
