﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ivit.db.Models
{
	public class Project
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid Id { get; set; }

		[Required] public string Title { get; set; }
		[Required] public string ShortDescription { get; set; }
		[Required] public string LongDescription { get; set; }
		[Required] public string ShortDescriptionPl { get; set; }
		[Required] public string LongDescriptionPl { get; set; }
		[Required] public string Role { get; set; }
		[Required] public string RolePl { get; set; }
		public virtual IEnumerable<FrontendTechnology> FrontendTechnologies { get; set; }
		public virtual IEnumerable<BackendTechnology> BackendTechnologies { get; set; }
		public virtual IEnumerable<Library> Libraries { get; set; }
		public virtual IEnumerable<Other> Others { get; set; }
		[Required] public DateTime StartDate { get; set; }
		public DateTime? EndDate { get; set; }

		public ProjectTypeEnum ProjectType { get; set; }

		[NotMapped] public string LogoFileName { get; set; }
		[NotMapped] public List<string> ScreensFileName { get; set; }
	}
}