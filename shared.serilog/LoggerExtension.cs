﻿using System;
using Serilog;
using Serilog.Events;

namespace shared.serilog
{
    public static class LoggerExtension
    {
        public static void Log(this ILogger logger, LogEventLevel level, string message = "", Exception exception = null, string userName = "")
        {
            switch (level)
            {
                case LogEventLevel.Verbose:
                    logger.Verbose(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
                case LogEventLevel.Debug:
                    logger.Debug(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
                case LogEventLevel.Information:
                    logger.Information(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
                case LogEventLevel.Warning:
                    logger.Warning(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
                case LogEventLevel.Error:
                    logger.Error(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
                case LogEventLevel.Fatal:
                    logger.Fatal(exception,
                        "{Message} {Exception_Message} {Exception_InnerExceptionMessage} {Exception_StackTrace} {Username}",
                        message, 
                        exception?.Message ?? "", 
                        exception?.InnerException?.Message ?? "", 
                        exception?.StackTrace ?? "", 
                        userName);
                    break;
            }
        }
    }
}
