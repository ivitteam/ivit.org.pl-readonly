﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Events;

namespace shared.serilog
{

    /// <summary>
    /// Custom logger working on Serilog. More info in readme.md
    /// </summary>
    public class CustomLogger : ICustomLogger
    {
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _context;

        public CustomLogger(ILogger logger, IHttpContextAccessor context)
        {
            _logger = logger;
            _context = context;
        }

        /// <summary>
        /// Logging verbose. Only to console.
        /// </summary>
        public void Verbose(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Verbose,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");

        /// <summary>
        /// Logging debug. Only to console.
        /// </summary>
        public void Debug(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Debug,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");

        /// <summary>
        /// Logging information. Only to console.
        /// </summary>
        public void Information(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Information,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");

        /// <summary>
        /// Logging warning. Only to console.
        /// </summary>
        public void Warning(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Warning,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");

        /// <summary>
        /// Logging error. Console & DB.
        /// </summary>
        public void Error(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Error,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");

        /// <summary>
        /// Logging fatal. Console & DB.
        /// </summary>
        public void Fatal(string message = "", Exception exception = null) => 
            _logger.Log(LogEventLevel.Fatal,
                message, 
                exception, 
                _context
                    ?.HttpContext
                    ?.User
                    ?.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.GivenName)
                    ?.Value 
                ?? "");
    }
}
