﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.MSSqlServer;

namespace shared.serilog
{
    public static class LoggerConfigurationExtension
    {
        private static LoggerConfiguration CreateLoggerConfiguration(string connectionString, string tableName)
        {
            var columnOptions = new ColumnOptions
            {
                AdditionalDataColumns = new Collection<DataColumn>
                {
                    new DataColumn {ColumnName = "UserName", DataType = typeof(string), AllowDBNull = true},
                    new DataColumn {ColumnName = "Exception_InnerExceptionMessage", DataType = typeof(string), AllowDBNull = true},
                    new DataColumn {ColumnName = "Exception_StackTrace", DataType = typeof(string), AllowDBNull = true},
                    new DataColumn {ColumnName = "Exception_Message", DataType = typeof(string), AllowDBNull = true}
                }
            };

            columnOptions.Store.Remove(StandardColumn.Message);
            columnOptions.Store.Remove(StandardColumn.MessageTemplate);
            columnOptions.Store.Remove(StandardColumn.Properties);

            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .WriteTo.Console()
                .WriteTo.MSSqlServer(
                    connectionString,
                    tableName,
                    LogEventLevel.Error,
                    autoCreateSqlTable: true,
                    columnOptions: columnOptions);
        }

        /// <summary>
        /// IServiceCollection extension method which adds ICustomLogger to your project
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="connectionStringName"></param>
        /// <param name="tableName">Database table name which will be added to main logs database</param>
        /// <param name="services"></param>
        public static void AddSerilogServices(this IServiceCollection services, IConfiguration configuration, string connectionStringName, string tableName)
        {
            Log.Logger = CreateLoggerConfiguration(configuration.GetConnectionString(connectionStringName), tableName)
                .CreateLogger();

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Log.CloseAndFlush();

            services.AddSingleton(Log.Logger);
            services.AddSingleton<ICustomLogger, CustomLogger>();
        }
    }
}