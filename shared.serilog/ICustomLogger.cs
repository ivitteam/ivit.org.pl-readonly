﻿using System;

namespace shared.serilog
{
    /// <summary>
    /// Custom logger working on Serilog. More info in readme.md
    /// </summary>
    public interface ICustomLogger
    {
        /// <summary>
        /// Logging verbose. Only to console.
        /// </summary>
        void Verbose(string message = "", Exception exception = null);
        /// <summary>
        /// Logging debug. Only to console.
        /// </summary>
        void Debug(string message = "", Exception exception = null);
        /// <summary>
        /// Logging information. Only to console.
        /// </summary>
        void Information(string message = "", Exception exception = null);
        /// <summary>
        /// Logging warning. Only to console.
        /// </summary>
        void Warning(string message = "", Exception exception = null);
        /// <summary>
        /// Logging error. Console & DB.
        /// </summary>
        void Error(string message = "", Exception exception = null);
        /// <summary>
        /// Logging fatal. Console & DB.
        /// </summary>
        void Fatal(string message = "", Exception exception = null);
    }
}