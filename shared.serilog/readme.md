﻿### Library: shared.serilog

##### Description:
Library is a custom logger builded on Serilog interfaces.

##### Usable interfaces
- ICustomLogger

##### Installation:

You can find existing implementation in *sfm.product* project. Installation steps below:

1. Include *shared.serilog* dll or project in your project.
2. Add connection string to log database in your *appsettings.json* file.
```JSON
"ConnectionStrings": {
    "LoggerConnection":
      "Data Source=.\\SQLEXPRESS;Initial Catalog=sfm-logs;Persist Security Info=True;User ID=sfm; Password=nNc^RjM!959Uj&"
  }
```
3. Add IServiceCollection extension method for *ICustomLogger* interface. Example below.
```C#
services.AddSerilogServices(Configuration, "table-name");
```
4. (optional) Add *HttpContextAccesor* in to your *IServiceCollection* if you want log with logged user username.
```C#
services.AddHttpContextAccessor();
```

##### Use example:

Logging with *ICustomLogger*

1. Create field in your class for interface:
```C#
private readonly ICustomLogger _logger;
```
2. Inject it with DI in your constructor:
```C#
public DeleteProductHandlerAsync(ICustomLogger logger)
{
    _logger = logger;
}
```
3. Use it when you want to log data. Console logs: Verbose, Debug, Information, Warning. DB logs: Error, Fatal.
```C#
catch (Exception e)
{
    var message = $"Something went wrong in: {GetType()}";
    _logger.Fatal(message, e);
    throw new HubException(message);
}
```




