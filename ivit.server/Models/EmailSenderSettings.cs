﻿namespace ivit.server.Models
{
	public class EmailSenderSettings
	{
		public string SmtpServer { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string ContactEmail { get; set; }
		public int Port { get; set; }
		public bool Ssl { get; set; }
	}
}