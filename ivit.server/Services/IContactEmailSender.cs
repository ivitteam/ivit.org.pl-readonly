﻿using System.Threading.Tasks;
using ivit.server.Models;

namespace ivit.server.Services
{
	public interface IContactEmailSender
	{
		Task SendContactEmail(ContactEmail message);
	}
}