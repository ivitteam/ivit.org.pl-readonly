﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ivit.server.Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;

namespace ivit.server.Services
{
	public class ContactEmailSender : IEmailSender, IContactEmailSender
	{
		private readonly IOptions<EmailSenderSettings> _configuration;

		public ContactEmailSender(IOptions<EmailSenderSettings> configuration)
		{
			_configuration = configuration;
		}

		public async Task SendContactEmail(ContactEmail message)
		{
			var client = new SmtpClient(_configuration.Value.SmtpServer)
			{
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(_configuration.Value.Login, _configuration.Value.Password),
				Port = _configuration.Value.Port,
				EnableSsl = _configuration.Value.Ssl
			};
			var mailMessage = new MailMessage
			{
				From = new MailAddress(message.Email)
			};
			mailMessage.To.Add(_configuration.Value.ContactEmail);
			mailMessage.Subject = $"Contact request - {message.FirstName} {message.LastName}";
			mailMessage.Body = message.Message;
			await client.SendMailAsync(mailMessage);
		}

		public Task SendEmailAsync(string email, string subject, string htmlMessage)
		{
			var client = new SmtpClient(_configuration.Value.SmtpServer)
			{
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(_configuration.Value.Login, _configuration.Value.Password),
				Port = _configuration.Value.Port,
				EnableSsl = _configuration.Value.Ssl
			};
			var mailMessage = new MailMessage
			{
				From = new MailAddress(_configuration.Value.ContactEmail)
			};
			mailMessage.To.Add(email);
			mailMessage.Subject = subject;
			mailMessage.Body = htmlMessage;
			return client.SendMailAsync(mailMessage);
		}
	}
}