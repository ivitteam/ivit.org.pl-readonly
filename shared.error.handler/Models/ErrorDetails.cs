﻿using Newtonsoft.Json;

namespace shared.error.handler.Models
{
    /// <summary>
    /// Model returned as JSON object to WebAPI request
    /// </summary>
    public class ErrorDetails
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
        
        public string StackTrace { get; set; }

        public string ExceptionMessage { get; set; }

        public string InnerExceptionMessage { get; set; }
 
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
