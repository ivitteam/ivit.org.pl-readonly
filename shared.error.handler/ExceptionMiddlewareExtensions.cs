﻿using Microsoft.AspNetCore.Builder;

namespace shared.error.handler
{
    public static class ExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Custom exception middleware for WebAPI projects
        /// </summary>
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<CustomExceptionMiddleware>();
        }
    }
}
