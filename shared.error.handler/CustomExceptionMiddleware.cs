﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using shared.error.handler.Models;
using shared.serilog;

namespace shared.error.handler
{
    /// <summary>
    /// Custom exception middleware for WebAPI projects. It catches global exceptions and handle them (logging to DB). Then it sends to request 500 response with error model inside.
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public class CustomExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICustomLogger _logger;

        public CustomExceptionMiddleware(RequestDelegate next, ICustomLogger logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.Fatal("Something went wrong", ex);
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(new ErrorDetails
            {
                StatusCode = context.Response.StatusCode,
                Message = "Internal Server Error",
                ExceptionMessage = exception.Message,
                StackTrace = exception.StackTrace,
                InnerExceptionMessage = exception.InnerException?.Message ?? ""
            }.ToString());
        }
    }
}
