﻿### Library: shared.error.handler

##### Description:
Library designed to handle global errors an log them to serilog. It is working as middleware. Actually designed only for WebApi.

##### Usable middlewares:
- ConfigureCustomExceptionMiddleware

##### Installation:

You can find existing implementation in *sfm.identity* project. Installation steps below:

1. Include *shared.error.handler* dll or project in your project.
2. Add Serilog Configuration to current config. Below code snippet added to mapper configuration.
```C#
//Serilog
services.AddSerilogServices(Configuration, "table name");
```

3. Add middleware to your *Configure* method. 
```C#
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
	app.ConfigureCustomExceptionMiddleware();
}
```

##### Use example:

It works automatically. When you throw unexpected exception in application it logs it using serilog to database and returns 500 request to WebApi.

1. Returned model
```C#
public class ErrorDetails
{
	public int StatusCode { get; set; }

	public string Message { get; set; }
        
	public string StackTrace { get; set; }

	public string ExceptionMessage { get; set; }
		
	public string InnerExceptionMessage { get; set; }         
}
```
2. Logged error in database
```SQL
Id	Level	TimeStamp	Exception	UserName	Exception_InnerExceptionMessage	Exception_StackTrace	Exception_Message
6	Fatal	2018-12-06 09:13:52.290	Microsoft.AspNetCore.SignalR.HubException: Something went wrong in: sfm.product.Application.Commands.Handlers.UpdateProductHandlerAsync
   at sfm.product.Application.Commands.Handlers.UpdateProductHandlerAsync.HandleAsync(UpdateProduct command) in C:\wwwroot\Inetpub\SFM\sfm-backend\sfm.product\Application\Commands\Handlers\UpdateProductHandlerAsync.cs:line 44
   at OpenCqrs.Commands.CommandSender.SendAndPublishAsync[TCommand,TAggregate](TCommand command)
   at sfm.product.Application.Hub.ProductHub.Update(Product model) in C:\wwwroot\Inetpub\SFM\sfm-backend\sfm.product\Application\Hub\ProductHub.cs:line 74	TESTDNMA		   at sfm.product.Application.Commands.Handlers.UpdateProductHandlerAsync.HandleAsync(UpdateProduct command) in C:\wwwroot\Inetpub\SFM\sfm-backend\sfm.product\Application\Commands\Handlers\UpdateProductHandlerAsync.cs:line 44
   at OpenCqrs.Commands.CommandSender.SendAndPublishAsync[TCommand,TAggregate](TCommand command)
   at sfm.product.Application.Hub.ProductHub.Update(Product model) in C:\wwwroot\Inetpub\SFM\sfm-backend\sfm.product\Application\Hub\ProductHub.cs:line 74	Something went wrong in: sfm.product.Application.Commands.Handlers.UpdateProductHandlerAsync
```

##### Angular implementation:
@Starek's job