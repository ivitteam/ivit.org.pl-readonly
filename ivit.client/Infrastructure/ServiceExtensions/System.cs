﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;

namespace ivit.client.Infrastructure.ServiceExtensions
{
    public static class System
    {
        public static void AddSystem(this IServiceCollection services)
        {
            services.AddLocalization(options =>
                options.ResourcesPath = "Resources");

            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
    }
}
