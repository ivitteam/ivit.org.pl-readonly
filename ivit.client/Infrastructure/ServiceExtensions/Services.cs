﻿using ivit.server.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ivit.client.Infrastructure.ServiceExtensions
{
    public static class Services
    {
        public static void AddServerServices(this IServiceCollection services)
        {
            services.AddSingleton<IContactEmailSender, ContactEmailSender>();
        }
    }
}
