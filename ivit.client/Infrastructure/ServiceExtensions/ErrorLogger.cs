﻿using shared.serilog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ivit.client.Infrastructure.ServiceExtensions
{
    public static class ErrorLogger
    {
        public static void AddErrorLogging(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.AddSerilogServices(configuration, "DefaultConnection", "Logs");
        }
    }
}
