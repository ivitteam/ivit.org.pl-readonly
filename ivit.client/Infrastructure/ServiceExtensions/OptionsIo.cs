﻿
using ivit.server.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ivit.client.Infrastructure.ServiceExtensions
{
    public static class OptionsIo
    {
        public static void AddOptionsExtension(this IServiceCollection services, IConfiguration configuration)
        {
            //IOptions - enabled
            services.AddOptions();

            //Configure IOptions<JwtSettings> from appsettings:JwtSettings section
            var emailSenderSettings = configuration.GetSection("EmailSender");
            services.Configure<EmailSenderSettings>(emailSenderSettings);
        }
    }
}
