﻿using Microsoft.AspNetCore.Builder;

namespace ivit.client.Infrastructure.AppBuilderExtensions
{
    public static class System
    {
        public static void ConfigureMvc(this IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc();
        }
    }
}
