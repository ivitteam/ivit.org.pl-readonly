﻿using System.Globalization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ivit.client.Infrastructure.AppBuilderExtensions
{
    public static class Globalization
    {
        public static void ConfigureGlobalization(this IApplicationBuilder app)
        {
            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("pl-PL")
            };

            var locOptions= app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            locOptions.Value.DefaultRequestCulture = new RequestCulture("en-US", "en-US");
            locOptions.Value.SupportedCultures = supportedCultures;
            locOptions.Value.SupportedUICultures = supportedCultures;

            app.UseRequestLocalization(locOptions.Value);
        }
    }
}
