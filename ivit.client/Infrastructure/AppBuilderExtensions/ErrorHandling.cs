﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using shared.error.handler;

namespace ivit.client.Infrastructure.AppBuilderExtensions
{
    public static class ErrorHandling
    {
        public static void ConfigureErrorHandling(this IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.ConfigureCustomExceptionMiddleware();
                app.UseHsts();
            }
        }
    }
}
