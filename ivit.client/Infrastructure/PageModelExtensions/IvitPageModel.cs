﻿using System;
using System.Threading.Tasks;
using ivit.server.Models;
using ivit.server.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ivit.client.Infrastructure.PageModelExtensions
{
	public class IvitPageModel : PageModel
	{
		private readonly IContactEmailSender _sender;

		public IvitPageModel(IContactEmailSender sender)
		{
			_sender = sender;
		}

		// ReSharper disable once UnusedMember.Global
		public ActionResult OnPostChangeCulture(string returnUrl, string returnQuerystring)
		{
			var culture = GetCulture() == "pl-PL" ? "en-US" : "pl-PL";

			Response.Cookies.Append(
				CookieRequestCultureProvider.DefaultCookieName,
				CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
				new CookieOptions
				{
					Expires = DateTimeOffset.UtcNow.AddYears(1),
					IsEssential = true,
					Path = "/",
					HttpOnly = false
				}
			);

			if (string.IsNullOrEmpty(returnQuerystring))
			{
				return RedirectToPage(returnUrl == "/" ? "" : returnUrl);
			}
			return Redirect(returnUrl == "/" ? "" : $"{returnUrl}{returnQuerystring}");
		}

		// ReSharper disable once UnusedMember.Global
		public async Task<IActionResult> OnPostSendMessage(ContactEmail model, string returnUrl)
		{
			await _sender.SendContactEmail(model);
			return RedirectToPage(returnUrl == "/" ? "" : returnUrl);
		}

		protected string GetCulture() => Request
			.HttpContext
			.Features
			.Get<IRequestCultureFeature>()
			.RequestCulture
			.Culture
			.ToString();
	}
}