﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.Infrastructure.PageModelExtensions
{
    public abstract class IvitViewComponent : ViewComponent
    {
        protected string GetCulture() => Request
            .HttpContext
            .Features
            .Get<IRequestCultureFeature>()
            .RequestCulture
            .Culture
            .ToString();
    }
}
