﻿using ivit.client.Infrastructure.PageModelExtensions;
using ivit.server.Services;

namespace ivit.client.Pages
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class IndexModel : IvitPageModel
	{
		public IndexModel(IContactEmailSender sender) : base(sender)
		{
		}

		public void OnGet()
		{
		}
	}
}