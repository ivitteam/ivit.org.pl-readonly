﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ivit.client.Infrastructure.PageModelExtensions;
using ivit.client.ViewModels;
using ivit.db;
using ivit.db.Models;
using ivit.db.Repository;
using ivit.server.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace ivit.client.Pages
{
	public class ProjectModel : IvitPageModel
	{
		public FullProjectViewModel Content { get; set; }
		public int ColumnsCounter { get; set; }

		private readonly IRepository<Project> _repository;
		private readonly IHostingEnvironment _env;

		public ProjectModel(IContactEmailSender sender, IRepository<Project> repository, IHostingEnvironment env) : base(sender)
		{
			_repository = repository;
			_env = env;
		}

		public async Task OnGet(Guid id)
		{
			var project = await _repository.GetSet()
				.Include(p => p.BackendTechnologies)
				.Include(p => p.FrontendTechnologies)
				.Include(p => p.Libraries)
				.Include(p => p.Others)
				.FirstOrDefaultAsync(p => p.Id == id);

			if (project != null)
			{
				var currentCulture = GetCulture();

				Content = new FullProjectViewModel
				{
					Role = currentCulture == "pl-PL" ? project.RolePl : project.Role,
					EndDate = project.EndDate.HasValue ? project.EndDate.Value.ToString("d") : currentCulture == "pl-PL" ? "teraz" : "now",
					LongDescription = currentCulture == "pl-PL" ? project.LongDescriptionPl : project.LongDescription,
					ShortDescription = currentCulture == "pl-PL" ? project.ShortDescriptionPl : project.ShortDescription,
					StartDate = project.StartDate.ToString("d"),
					Title = project.Title,
					Type = project.ProjectType.ToString()
				};

				//LOGO PART
				var logoPath = Path.Combine(_env.WebRootPath, "image", project.Id.ToString("D"), "logo.png");
				if (System.IO.File.Exists(logoPath))
				{
					Content.LogoFileName = $"image/{project.Id:D}/logo.png";
				}
				else
				{
					switch (project.ProjectType)
					{
						case ProjectTypeEnum.Web:
							Content.LogoFileName = "image/www.png";
							break;
						case ProjectTypeEnum.Desktop:
							Content.LogoFileName = "image/desktop.png";
							break;
						case ProjectTypeEnum.Other:
							Content.LogoFileName = "image/other.png";
							break;
						default:
							Content.LogoFileName = "image/www.png";
							break;
					}
				}

				//SCREENS PART
				var pathScreens = Path.Combine(_env.WebRootPath, "image", project.Id.ToString("D"));
				var dirScreens = new DirectoryInfo(pathScreens);

				if (dirScreens.Exists && dirScreens.EnumerateFiles().Any(p => p.Name != "logo.png"))
				{
					Content.ScreensFileName = dirScreens.EnumerateFiles().Where(p => p.Name != "logo.png").Select(screen => $"image/{project.Id:D}/{screen.Name}").ToList();
				}

				ColumnsCounter = 0;

				//BACKEND PART
				Content.BackendTechnologies = project.BackendTechnologies
					.Select(p => currentCulture == "pl-PL" ? p.DescriptionPl : p.Description).ToList();

				if (Content.BackendTechnologies.Any()) ColumnsCounter++;

				//FRONTEND PART
				Content.FrontendTechnologies = project.FrontendTechnologies
					.Select(p => currentCulture == "pl-PL" ? p.DescriptionPl : p.Description).ToList();

				if (Content.FrontendTechnologies.Any()) ColumnsCounter++;

				//LIBRARY PART
				Content.Libraries = project.Libraries
					.Select(p => currentCulture == "pl-PL" ? p.DescriptionPl : p.Description).ToList();

				if (Content.Libraries.Any()) ColumnsCounter++;

				//OTHER PART
				Content.Others = project.Others
					.Select(p => currentCulture == "pl-PL" ? p.DescriptionPl : p.Description).ToList();

				if (Content.Others.Any()) ColumnsCounter++;

			}
		}
	}
}