﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ivit.client.Infrastructure.PageModelExtensions;
using ivit.client.ViewModels;
using ivit.db;
using ivit.db.Models;
using ivit.db.Repository;
using ivit.server.Services;
using Microsoft.AspNetCore.Hosting;

namespace ivit.client.Pages
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class PortfolioModel : IvitPageModel
	{
		private readonly IRepository<Project> _repository;
		private readonly IHostingEnvironment _env;

		public IEnumerable<PortfolioElementViewModel> Content { get; set; }

		public PortfolioModel(IContactEmailSender sender, IRepository<Project> repository, IHostingEnvironment env) : base(sender)
		{
			_repository = repository;
			_env = env;
		}

		public async Task OnGet()
		{
			var projectList = await _repository.FindAllAsync();
			var enumerableProjectList = projectList.ToList();

			var projects = enumerableProjectList.Where(p => p.EndDate == null).OrderBy(p => p.EndDate).ThenByDescending(p => p.StartDate).ToList();
			projects.AddRange(enumerableProjectList.Where(p => p.EndDate != null).OrderByDescending(p => p.EndDate));

			var model = new List<PortfolioElementViewModel>();
			var currentCulture = GetCulture();
			foreach (var project in projects)
			{
				var viewModel = new PortfolioElementViewModel
				{
					Id = project.Id,
					Title = project.Title,
					ShortDesc = currentCulture == "pl-Pl" ? project.ShortDescriptionPl : project.ShortDescription,
					Type = project.ProjectType
				};

				var logoPath = Path.Combine(_env.WebRootPath, "image", project.Id.ToString("D"), "logo.png");

				if (System.IO.File.Exists(logoPath))
				{
					viewModel.LogoPath = $"image/{project.Id:D}/logo.png";
				}
				else
				{
					switch (project.ProjectType)
					{
						case ProjectTypeEnum.Web:
							viewModel.LogoPath = "image/www.png";
							break;
						case ProjectTypeEnum.Desktop:
							viewModel.LogoPath = "image/desktop.png";
							break;
						case ProjectTypeEnum.Other:
							viewModel.LogoPath = "image/other.png";
							break;
						default:
							viewModel.LogoPath = "image/www.png";
							break;
					}
				}

				model.Add(viewModel);
			}

			Content = model;
		}
	}
}