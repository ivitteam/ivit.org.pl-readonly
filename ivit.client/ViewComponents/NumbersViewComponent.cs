﻿using ivit.client.Infrastructure.PageModelExtensions;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class NumbersViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
