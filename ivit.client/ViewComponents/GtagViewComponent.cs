﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
	public class GtagViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View();
		}
	}
}
