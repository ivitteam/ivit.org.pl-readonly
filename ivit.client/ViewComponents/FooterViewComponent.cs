﻿using ivit.client.Infrastructure.PageModelExtensions;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class FooterViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}
