﻿using ivit.client.Infrastructure.PageModelExtensions;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class HeadViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}
