﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class LanguageViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
