﻿using ivit.client.Infrastructure.PageModelExtensions;
using ivit.client.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
	public class TitleViewComponent : IvitViewComponent
	{
		public ProjectTitleViewModel Content { get; set; }

		// ReSharper disable once EmptyConstructor
		public TitleViewComponent()
		{
		}

		public IViewComponentResult Invoke(string title, string imagePath)
		{
			Content = new ProjectTitleViewModel
			{
				Title = title,
				LogoPath = imagePath
			};
			return View(this);
		}
	}
}