﻿using System.Collections.Generic;
using ivit.client.Infrastructure.PageModelExtensions;
using ivit.db.Models;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class SkillViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke(Skill model)
        {
            switch (GetCulture())
            {
                case "pl-PL": return View(new KeyValuePair<int, string>(model.Percent, model.PolishText));
                default: return View(new KeyValuePair<int, string>(model.Percent, model.EnglishText));
            }
        }
    }
}
