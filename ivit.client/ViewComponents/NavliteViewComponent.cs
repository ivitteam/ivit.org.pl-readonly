﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class NavliteViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
           return View();
        }
    }
}
