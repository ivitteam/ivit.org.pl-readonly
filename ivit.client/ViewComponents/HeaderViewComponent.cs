﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
