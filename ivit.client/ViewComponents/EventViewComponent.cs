﻿using System.Collections.Generic;
using ivit.client.Infrastructure.PageModelExtensions;
using ivit.db.Models;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class EventViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke(TimelineEvent model)
        {
            switch (GetCulture())
            {
                case "pl-PL": return View(new KeyValuePair<string, string>(model.StartYear.ToString(), model.PolishText));
                default: return View(new KeyValuePair<string, string>(model.StartYear.ToString(), model.EnglishText));
            }
        }
    }
}
