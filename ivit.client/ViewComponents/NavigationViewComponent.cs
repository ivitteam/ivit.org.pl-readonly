﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class NavigationViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
           return View();
        }
    }
}
