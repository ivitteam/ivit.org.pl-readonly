﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ivit.client.Infrastructure.PageModelExtensions;
using ivit.client.ViewModels;
using ivit.db;
using ivit.db.Models;
using ivit.db.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
	public class ReferenceViewComponent : IvitViewComponent
	{
		private readonly IRepository<Project> _repository;
		private readonly IHostingEnvironment _env;

		public ReferenceViewComponent(IRepository<Project> repository, IHostingEnvironment env)
		{
			_repository = repository;
			_env = env;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var projectList = await _repository.FindAllAsync();
			var enumerableProjectList = projectList.ToList();

			var projects = enumerableProjectList.Where(p => p.EndDate == null).OrderBy(p => p.EndDate).ThenByDescending(p => p.StartDate).Take(6).ToList();
			var takeCount = 6 - projects.Count;
			projects.AddRange(enumerableProjectList.Where(p => p.EndDate != null).OrderByDescending(p => p.EndDate)
				.Take(takeCount));

			var model = new List<ProjectViewModel>();

			foreach (var project in projects)
			{
				var viewModel = new ProjectViewModel
				{
					Id = project.Id,
					Title = project.Title
				};

				var logoPath = Path.Combine(_env.WebRootPath, "image", project.Id.ToString("D"), "logo.png");

				if (File.Exists(logoPath))
				{
					viewModel.LogoPath = $"image/{project.Id:D}/logo.png";
				}
				else
				{
					switch (project.ProjectType)
					{
						case ProjectTypeEnum.Web:
							viewModel.LogoPath = "image/www.png";
							break;
						case ProjectTypeEnum.Desktop:
							viewModel.LogoPath = "image/desktop.png";
							break;
						case ProjectTypeEnum.Other:
							viewModel.LogoPath = "image/other.png";
							break;
						default:
							viewModel.LogoPath = "image/www.png";
							break;
					}
				}

				model.Add(viewModel);
			}

			return View(model);
		}
	}
}