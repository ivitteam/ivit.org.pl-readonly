﻿using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class ContactViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
