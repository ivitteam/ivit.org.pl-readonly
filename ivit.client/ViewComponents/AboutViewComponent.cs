﻿using System.Linq;
using System.Threading.Tasks;
using ivit.client.ViewModels;
using ivit.db.Models;
using ivit.db.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class AboutViewComponent : ViewComponent
    {
        private readonly IRepository<TimelineEvent> _eventRepository;
        private readonly IRepository<Skill> _skillRepository;

        public AboutViewComponent(IRepository<TimelineEvent> eventRepository, IRepository<Skill> skillRepository)
        {
            _eventRepository = eventRepository;
            _skillRepository = skillRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var events = await _eventRepository.FindAllAsync();
            var skills = await _skillRepository.FindAllAsync();

            var model = new AboutViewComponentModel
            {
                Events = events.OrderBy(p => p.StartYear),
                Skills = skills.OrderByDescending(p => p.Percent)
            };
            return View(model);
        }
    }
}
