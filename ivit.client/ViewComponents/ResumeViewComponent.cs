﻿using ivit.client.Infrastructure.PageModelExtensions;
using Microsoft.AspNetCore.Mvc;

namespace ivit.client.ViewComponents
{
    public class ResumeViewComponent : IvitViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}
