﻿namespace ivit.client.ViewModels
{
	public class ProjectTitleViewModel
	{
		public string Title { get; set; }
		public string LogoPath { get; set; }
	}
}
