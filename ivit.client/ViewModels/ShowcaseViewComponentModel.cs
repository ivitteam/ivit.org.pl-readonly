﻿using System;
using System.Collections.Generic;

namespace ivit.client.ViewModels
{
    public class ShowcaseViewComponentModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string ShortDesc { get; set; }
        public string Backend { get; set; }
        public string Frontend { get; set; }
        public string Other { get; set; }
        public string Libraries { get; set; }
        public List<string> Images { get; set; }
    }
}
