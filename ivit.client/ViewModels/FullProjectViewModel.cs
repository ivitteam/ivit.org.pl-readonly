﻿using System.Collections.Generic;

namespace ivit.client.ViewModels
{
	public class FullProjectViewModel
	{
		public string Title { get; set; }
		public string ShortDescription { get; set; }
		public string LongDescription { get; set; }
		public string Role { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string Type { get; set; }
		public List<string> FrontendTechnologies { get; set; }
		public List<string> BackendTechnologies { get; set; }
		public List<string> Libraries { get; set; }
		public List<string> Others { get; set; }
		public string LogoFileName { get; set; }
		public List<string> ScreensFileName { get; set; }
	}
}
