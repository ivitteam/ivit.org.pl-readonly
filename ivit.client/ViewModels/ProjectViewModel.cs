﻿using System;

namespace ivit.client.ViewModels
{
	public class ProjectViewModel
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string LogoPath { get; set; }

	}
}
