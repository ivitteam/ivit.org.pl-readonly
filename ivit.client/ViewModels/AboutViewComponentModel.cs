﻿using System.Collections.Generic;
using ivit.db.Models;

namespace ivit.client.ViewModels
{
	public class AboutViewComponentModel
	{
		public IEnumerable<TimelineEvent> Events { get; set; }
		public IEnumerable<Skill> Skills { get; set; }
	}
}