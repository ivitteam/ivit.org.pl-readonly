﻿using System;
using ivit.db;

namespace ivit.client.ViewModels
{
	public class PortfolioElementViewModel
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string LogoPath { get; set; }
		public ProjectTypeEnum Type { get; set; }
		public string ShortDesc { get; set; }
	}
}
