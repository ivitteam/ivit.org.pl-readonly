using ivit.client.Infrastructure.AppBuilderExtensions;
using ivit.client.Infrastructure.ServiceExtensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ivit.client
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddWebConfiguration(Configuration);
            services.AddOptionsExtension(Configuration);
            services.AddErrorLogging(Configuration);
            services.AddIvitDb(Configuration);
            services.AddServerServices();
            services.AddSystem();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.ConfigureErrorHandling(env);
            app.ConfigureGlobalization();
            app.ConfigureMvc();
        }
    }
}
